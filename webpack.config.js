const path = require('path')

module.exports = {
  entry: './src/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname,'dist')
  },
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
        {
          test: /\.css$/,
          use: ['style-loader','css-loader']
        },
        {
          test: /\.(jpg||gif||png||svg)$/,
          use: 'file-loader'
        },
        {
          test: /\.m?jsx$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        }
    ]
  }
}
