import React from "react"
import { graphql } from "gatsby"

import {Content, Header, SideBar, Footer} from "../components"


const NotFoundPage = ({ data }) => {
  const siteTitle = data.site.siteMetadata.title
  return (
      <h1>Байланышсыз калдыӊыз</h1>
      <p>Байланшыӊызды кайрадан текшерип көрүӊүз</p>
  );
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`