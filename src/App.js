import React, {Component} from "react";
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';

import {Header, Menu, NavToogle, Search} from '../components/Header';
import {Content} from '../components/content';
import {SideBar, SideBarContent, SideBarForm, SideBarPhoto, SideBarVideo} from '../components/SideBar';
import {Footer} from '../components/footer';
import {store} from './store';

export const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
      <header>
          <Link to="/">Блог</Link>
          &nbsp;
          <Link to="/Кыргызча IT">Кыргызча IT</Link>
        </header>
        <br />
        <main>
        <Content />
        <SideBar />
        <Footer />
          <Switch>
            <Route path='/' component={Блог} />  
            <Route path='/Кыргызча IT' component={Кыргызча IT} />  
          <Switch>
        </main>
      </BrowserRouter>
    </Provider>
  )
};


