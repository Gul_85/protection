import React from "react";
import "./SideBarContent.module.css";

class SideBarPhoto extends React.Component{
  render () {
    return (
        <div className="widget">
          <h3 className="widget-title__last">Фоторепортаж</h3>
            <ul className="widget-posts-list">
          <li>
            <div className="post-image-small">
            <a href=""><img src="/" /></a>
            </div>
          <h4 className="widget-post-title"><a href=""></a></h4>
          </li>
        </ul>
      </div>
    );
  }
};

  
export default SideBar;
