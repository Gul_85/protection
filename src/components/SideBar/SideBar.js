import React from "react"
import ReactDOM from "react-dom"

import "../components/SideBar.css";

class SideBar extends React.Component{
  render () {
    return (
      <SideBar>
        <SideBarContent />
        <SideBarVideo />
        <SideBarPhoto />
        <SideBarForm />
    </SideBar>
    );
  }
};

export default App;

