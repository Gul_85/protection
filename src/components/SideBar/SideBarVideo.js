import React from "react";

import "./SideBarVideo.css";


const SideBarVideo = ({ videoSrcURL, videoTitle, ...props }) => (
  <div className="video">
    <div class="videos__wrapper">
    <iframe
      src={videoSrcURL}
      title={videoTitle}
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      frameBorder="0"
      webkitallowfullscreen="true"
      mozallowfullscreen="true"
      allowFullScreen
    />
  </div>
  </div>
);

export default SideBar;