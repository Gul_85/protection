import React from 'react'
import {Nav, NavItem} from 'react-bootstrap'

import '../components/menu.css'

class Menu extends React.Component {
  render () {
    return (
      <Nav>
        <NavItem eventKey={1} href="/Блог">Блог</NavItem>
        <NavItem eventKey={2} href="/Кыргызча IT">Кыргызча IT</NavItem>
        <NavItem eventKey={3} href="/Жапон борбор">Жапон борбор</NavItem>
      </Nav>
    );
  }
}

export default Header;