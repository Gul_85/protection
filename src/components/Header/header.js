import React from 'react'
import ReactDOM from "react-dom"

import '../components/header.module.css'


class Header extends React.Component{
  render () {
    return (
      <Header className='header-container' inverse fixedTop>
        <span className='logo'><kbd>C</kbd></span>
        <span className='logo'><kbd>А</kbd></span>
        <span className='logo'><kbd>Л</kbd></span>
        <span className='logo'><kbd>Ы</kbd></span>
        <span className='logo'><kbd>М</kbd></span>
        <NavToogle />
        <Menu />
        <Search />
      </Header>
    );
  }
}

export default App;